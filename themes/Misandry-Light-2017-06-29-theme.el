(deftheme Misandry-Light-2017-06-29
  "Created 2017-06-29.")

(custom-theme-set-faces
 'Misandry-Light-2017-06-29
 '(font-lock-builtin-face ((t (:foreground "dark violet"))))
 '(font-lock-comment-face ((t (:foreground "dark green"))))
 '(font-lock-doc-face ((t (:foreground "orange4"))))
 '(font-lock-function-name-face ((t (:foreground "dark slate blue"))))
 '(font-lock-keyword-face ((t (:foreground "magenta3"))))
 '(font-lock-negation-char-face ((t (:foreground "orange4"))))
 '(font-lock-regexp-grouping-backslash ((t (:foreground "blue4"))))
 '(font-lock-regexp-grouping-construct ((t (:foreground "DodgerBlue4"))))
 '(font-lock-string-face ((t (:foreground "DeepSkyBlue4"))))
 '(font-lock-type-face ((t (:foreground "dark magenta"))))
 '(font-lock-variable-name-face ((t (:foreground "dark slate gray"))))
 '(header-line ((t (:box (:line-width 2 :color "#fdf6e3" :style unspecified) :foreground "black" :background "#fdf6e3"))))
 '(minibuffer-prompt ((t (:foreground "dark red"))))
 '(mode-line ((((class color) (min-colors 89)) (:background "PaleGoldenrod" :foreground "black" :box (:line-width -1 :style released-button)))))
 '(mode-line-inactive ((((class color) (min-colors 89)) (:inherit mode-line :background "LightGray" :foreground "grey20" :box (:line-width -1 :color "grey75") :weight light))))
 '(outline-1 ((t (:inherit font-lock-function-name-face :weight bold))))
 '(outline-2 ((t (:inherit font-lock-variable-name-face :weight bold))))
 '(outline-3 ((t (:inherit font-lock-keyword-face :weight bold))))
 '(outline-4 ((t (:inherit font-lock-comment-face :weight bold))))
 '(outline-5 ((t (:inherit font-lock-type-face :weight bold))))
 '(outline-6 ((t (:inherit font-lock-constant-face :weight bold))))
 '(outline-7 ((t (:inherit font-lock-builtin-face :weight bold))))
 '(outline-8 ((t (:inherit font-lock-string-face :weight bold))))
 '(region ((((class color) (min-colors 89)) (:background "MediumAquamarine"))))
 '(show-paren-match ((t (:background "Cyan1" :weight bold))))
 '(show-paren-mismatch ((t (:background "deep pink" :weight bold))))
 '(cursor ((t (:background "light sea green"))))
 '(fixed-pitch ((t (:family "pointfree"))))
 '(escape-glyph ((((class color) (min-colors 89)) (:background "gold" :foreground "blue" :box (:line-width 1 :color "blue" :style released-button)))))
 '(highlight ((((class color) (min-colors 89)) (:background "cyan"))))
 '(shadow ((t (:foreground "#93a1a1"))))
 '(secondary-selection ((((class color) (min-colors 89)) (:background "white" :foreground "black"))))
 '(trailing-whitespace ((t (:foreground "LightSalmon3" :box (:line-width 1 :color "LightSalmon3" :style released-button)))))
 '(font-lock-constant-face ((((class color) (min-colors 89)) (:foreground "#00006DE06DE0"))))
 '(font-lock-preprocessor-face ((t (:foreground "CadetBlue4"))))
 '(font-lock-warning-face ((t (:foreground "dark goldenrod" :weight bold))))
 '(button ((t (:underline (:color foreground-color :style line)))))
 '(link-visited ((t (:inherit link :foreground "VioletRed1"))))
 '(fringe ((((class color) (min-colors 89)) (:background "gray85"))))
 '(tooltip ((t (:box (:line-width 2 :color "spring green" :style released-button)))))
 '(mode-line-buffer-id ((((class color) (min-colors 89)) (:overline "red" :underline "red"))))
 '(mode-line-emphasis ((t (:slant italic))))
 '(mode-line-highlight ((t (:box (:line-width 1 :color "misty rose" :style released-button)))))
 '(isearch ((((class color) (min-colors 89)) (:background "green" :foreground "Black"))))
 '(isearch-fail ((t (:background "#fdf6e3" :foreground "#dc322f"))))
 '(lazy-highlight ((((class color) (min-colors 89)) (:background "dark turquoise"))))
 '(match ((((class color) (min-colors 89)) (:background "SkyBlue"))))
 '(homoglyph ((((class color) (min-colors 89)) (:background "gold" :foreground "blue" :box (:line-width 1 :color "blue" :style released-button)))))
 '(error ((default (:weight bold)) (((class color) (min-colors 88) (background light)) (:foreground "Red1")) (((class color) (min-colors 88) (background dark)) (:foreground "Pink")) (((class color) (min-colors 16) (background light)) (:foreground "Red1")) (((class color) (min-colors 16) (background dark)) (:foreground "Pink")) (((class color) (min-colors 8)) (:foreground "red")) (t (:inverse-video t))))
 '(link ((t (:foreground "dark blue" :underline t))))
 '(default ((t (:background "mint cream" :foreground "black" :height 100 :family "TeX Gyre Pagella")))))

(provide-theme 'Misandry-Light-2017-06-29)
