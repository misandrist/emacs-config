;;; init-async.el --- Configure async mode.          -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, extensions, local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'use-package)

(use-package async
  :ensure async
  :commands (async-start-process async-start)
  )

(use-package async-bytecomp
  :ensure async
  :pin manual
  :init (setq async-bytecomp-allowed-packages '(all))
  :commands (async-byte-recompile-directory async-bytecomp-package-mode)
  :config (async-bytecomp-package-mode 1)
  )

(use-package dired-async
  :ensure async
  :pin manual
  :init
  (add-hook 'dired-mode-hook #'dired-async-mode)
  :commands (dired-async-mode)
  )

(provide 'init-async)
;;; init-async.el ends here
