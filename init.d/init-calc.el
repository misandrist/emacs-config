;;; init-calc.el --- Initialize calc mode.           -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initializes `calculator' mode.

;;; Code:

(require 'use-package)

(use-package calculator
  :commands (calculator)
  :init
  (setq calculator-electric-mode t
        calculator-use-menu t
        calculator-bind-escape t
        calculator-unary-style 'prefix
        calculator-prompt "Calc ⟨%s⟩ → "
        calculator-number-digits 6
        calculator-radix-grouping-mode t
        calculator-radix-grouping-digits 4
        calculator-radix-grouping-separator "❘"
        calculator-remove-zeros 'leave-decimal
        calculator-2s-complement t
        )
  :bind (("<XF86Calculator>" . calculator))
  )

(provide 'init-calc)
;;; init-calc.el ends here
