;;; init-browser.el --- Configure `browse-url' to taste.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, files, hypermedia, multimedia, tools, processes

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This configures the `browse-url' package to use Chromium in X, and
;; elinks at the terminal.

;;; Code:

(require 'use-package)

(use-package browse-url
  :init
  (setq browse-url-browser-function
        (if (window-system)
            #'browse-url-chromium
          #'browse-url-elinks)
        )
  :commands (browse-url-chromium browse-url-elinks browse-url browse-url-mail)
  )

(provide 'init-browser)
;;; init-browser.el ends here
