;;; init-biblio.el --- Initialize `biblio' and define `async' operations.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: bib, hypermedia, tex, unix, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This configures the `biblio' package. This also defines several
;; `async' wrappers around the lookup functions to keep things
;; interactive.
;;

;;; Code:

(defgroup tum-biblio nil
  "The UNIX Man papers and bibliographies archive."
  :group 'tum-emacs)

(defcustom tum-biblio-archive "/projects/misandrist/Papers/"
  "Directory relative where papers are stored."
  :group 'tum-biblio
  :type 'directory
  :set #'tum-custom-mkdirs
  )

(defcustom tum-biblio-bibliography "/projects/misandrist/bibliography.bib"
  "The BiBTeX file where all citations are stored."
  :group 'tum-biblio
  :type 'file
  :set #'tum-custom-f-touch
  )

(defcustom tum-biblio-notes "/projects/misandrist/Notes/"
  "The Notes directory for `bibtex-completion-notes-path'."
  :group 'tum-biblio
  :type 'directory
  :set #'tum-custom-mkdirs
  )

(use-package biblio
  :ensure t
  :init
  (setq biblio-download-directory tum-biblio-archive)
  :commands (crossref-lookup arxiv-lookup dblp-lookup doi-insert dissemin-lookup)
  )

(provide 'init-biblio)
;;; init-biblio.el ends here
