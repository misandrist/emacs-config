;;; init-compilation.el --- Set up compilation modes.  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: processes, tools, abbrev

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'use-package)

(use-package irony
  :ensure t
  )

(use-package irony-eldoc
  :ensure t
  )

(provide 'init-compilation)

;;; init-compilation.el ends here
