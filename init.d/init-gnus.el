;;; init-gnus.el --- Configure `gnus' for all our e-mail.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: comm

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Configures `gnus' with various IMAP and SMTP accounts.

;;; Code:

(require 'use-package)

(use-package gnus
  :init
  (setq mail-sources
        '((imap
           :server "mail.theunixman.com"
           :stream starttls
           :expunge nil
           :fetchflag "\\Seen")
          )
        nnmail-cache-accepted-message-ids t
        message-send-mail-function 'smtpmail-send-it
        smtpmail-default-smtp-server "mail.theunixman.com"
        gnus-select-method '(nnimap "mail.theunixman.com")
        )
  )

(provide 'init-gnus)
;;; init-gnus.el ends here
