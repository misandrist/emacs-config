;;; init-org-mode.el --- Configure org mode          -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize org-mode and its modules.

;;; Code:

(require 'use-package)

(defvar tum-global-org-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "l") #'org-store-link)
    (define-key map (kbd "a") #'org-agenda)
    (define-key map (kbd "c") #'org-capture)
    (define-key map (kbd "b") #'org-iswitchb)
    map
    )
  "The global `org-mode' commands for a prefix key.")

(define-prefix-command 'tum-global-org-mode-map)

(use-package org
  :ensure t
  :init
  (setq
   org-catch-invisible-edits 'smart
   org-ctrl-k-protect-subtree f
   org-deadline-warning-days 4
   org-default-notes-file (concat (emacs-run-data-dir "org-mode" "notes") "notes.org")
   org-descriptive-links t
   org-directory (emacs-run-data-dir "org-mode" "org-files")
   org-display-internal-link-with-indirect-buffer t
   org-edit-timestamp-down-means-later t
   org-ellipsis "…"
   org-enforce-todo-checkbox-dependencies t
   org-enforce-todo-dependencies t
   org-fontify-done-headline t
   org-fontify-whole-heading-line t
   org-hide-emphasis-markers t
   org-hide-macro-markers t
   org-highlight-latex-and-related '('latex 'script 'entities)
   org-image-actual-width 300
   org-insert-heading-respect-content t
   org-insert-mode-line-in-empty-file t
   org-keep-stored-link-after-insertion t
   org-log-done 'note
   org-log-into-drawer t
   org-log-note-clock-out t
   org-log-redeadline 'note
   org-log-refile 'note
   org-log-repeat 'note
   org-log-reschedule 'note
   org-log-states-order-reversed t
   org-loop-over-headlines-in-active-region 'start-level
   org-outline-path-complete-in-steps t
   org-pretty-entities t
   org-pretty-entities-include-sub-superscripts t
   org-preview-latex-image-directory ".ltximg/"
   org-refile-active-region-within-subtree t
   org-refile-allow-creating-parent-nodes 'confirm
   org-refile-use-cache t
   org-refile-use-outline-path 'full-file-path
   org-replace-disputed-keys t
   org-self-insert-cluster-for-undo t
   org-special-ctrl-a/e 'reversed
   org-special-ctrl-k t
   org-startup-indented t
   org-startup-with-inline-images t
   org-startup-with-latex-preview t
   org-track-ordered-property-with-tag t
   org-treat-S-cursor-todo-selection-as-state-change t
   org-treat-insert-todo-heading-as-state-change t
   org-use-extra-keys t
   org-use-sub-superscripts nil
   org-yank-adjusted-subtrees t
   )
  :commands (org-store-link
             org-insert-link-global
             org-open-at-point-global
             org-open-link-from-string
             org-switchb
             org-cycle-agenda-files
             )
  :bind (("M-o" . tum-global-org-mode-map))
  :config
  (add-hook 'org-mode-hook #'org-clock-persistence-insinuate)
  (add-hook 'org-mode-hook #'org-bullets-mode)
  (add-hook 'org-mode-hook #'org-ac/config-default)
  (add-hook 'org-mode-hook #'org-autolist-mode)
  )

(use-package org-bullets
  :ensure t
  :commands org-bullets-mode
  )

(use-package org-capture
  :ensure t
  :init
  (setq
   org-capture-use-agenda-date t
   )
  :commands (org-capture org-capture-minor-mode org-capture-string)
  )

(use-package org-ac
  :ensure t
  :commands (org-ac/config-default org-ac/setup-current-buffer)
  )

(use-package org-download
  :ensure t
  )

(use-package org-ref
  :ensure t
  :init
  (setq
   org-ref-bibliography-notes-directory "/projects/misandrist/Papers/Notes/"
   org-ref-default-bibliography (list "/projects/misandrist/Papers/bibliography.bib")
   org-ref-completion-library 'org-ref-helm-bibtex
   )
  :commands
  (org-ref-index
   org-ref-show-link-messages
   org-ref-cancel-link-messages
   org-ref-change-completion
   org-ref-mouse-message
   org-ref-mouse-messages-on
   org-ref-mouse-messages-off
   org-ref-insert-bibliography-link
   org-ref-list-of-figures
   org-ref-list-of-tables
   org-ref-insert-ref-link
   org-pageref-insert-ref-link
   org-ref-define-citation-link
   org-ref-insert-cite-with-completion
   org-ref-store-bibtex-entry-link
   org-ref-indexorg-ref-open-bibtex-pdf
   org-ref-open-bibtex-notes
   org-ref-open-in-browser
   org-ref-build-full-bibliography
   org-ref-extract-bibtex-entries
   org-ref-find-bad-citations
   org-ref-sort-bibtex-entry
   org-ref-downcase-bibtex-entry
   org-ref-clean-bibtex-entry
   org-ref-sort-citation-link
   org-ref-swap-citation-link
   org-ref-next-key
   org-ref-previoous-key
   org-ref-link-message
   org-ref-helm-insert-label-link
   org-ref-add-glossary-entry
   org-ref-add-acronym-entry
   org-ref-insert-glossary-link
   org-ref-helm-insert-ref-link
   org-ref-helm
   helm-tag-bibtex-entry
   org-ref-bibtex-completion-completion
   org-ref-helm-load-completions-async
   org-ref-helm-insert-cite-link
   org-ref-cite-click-helm
   org-ref-browser
   )
  )

;; (use-package org-repo-todo
;;   :ensure t
;;   :commands (ort/capture-todo ort/capture-checkitem ort/goto-todos)
;;   :after org
;;   :bind (
;;          ("C-;" . #'ort/capture-todo)
;;          ("C-'" . #'ort/capture-checkitem)
;;          ("C-`" . #'ort/goto-todos))
;;   )


(use-package org-autolist
  :ensure t
  :commands org-autolist-mode
  )

(use-package org-bookmark-heading
  :ensure t
  :after org
  )

(use-package org-cliplink
  :ensure t
  :after org
  :commands org-cliplink
  )

(use-package org-dashboard
  :ensure t
  :after org
  :commands org-dashboard-display
    )

(use-package org-context
  :ensure t
  :commands org-context-activate
  :after org
  )

(use-package org-doing
  :ensure org

  )

(defvar org-mobile-rsync-flags
  (list "--archive" "--no-owner")
  "The flags to pass to rsync when syncing with `org-mobile-push' and `org-mobile-pull'")

(defvar org-mobile-remote-path
  "serenity.theunixman.com:/users/sites/theunixman/htdocs/.mobile-org/evan/"
  "The remote host and path for rsync for `org-mobile-push' and `org-mobile-pull'")

(defvar org-mobile-rsync-log-dir
  (emacs-run-data-dir "org-mobile" "log")

  "Path for `org-mobile-rsync' log files.")

(defun org-mobile-rsync-log-file ()
    "Returns a path to a new log file for `org-mobile-rsync'."

  (concat org-mobile-rsync-log-dir (format-time-string "%FT%T.%N.log"))
  )

(defun org-mobile-rsync (src dst)
  "Run rsync with `org-mobile-rsync-flags' from `src' to `dst'"

  (let ((rlg (org-mobile-rsync-log-file)))
    (message "org-mobile: Starting rsync from %s to %s: logging to %s" src dst rlg)
    (call-process
     "rsync" nil nil nil
     org-mobile-rsync-flags
     "--log-file" rlg
     src dst)
    (message "org-mobile: Completed rsync from %s to %s: logging to %s" src dst rlg)
    )
  )

(use-package org-mobile
  :ensure t
  :commands (org-mobile-pull org-mobile-push)
  :init
  (setq org-mobile-directory (emacs-run-data-dir "org-mobile" "push")
        org-mobile-inbox-for-pull (emacs-run-data-dir "org-mobile" "pull"))

  (add-hook 'org-mobile-post-push-hook (lambda () (org-mobile-rsync org-mobile-directory org-mobile-remote-path)))
  (add-hook 'org-mobile-pre-pull-hook (lambda () (org-mobile-rsync org-mobile-remote-path org-mobile-directory)))

  (add-hook 'org-mobile-post-pull-hook (lambda () (org-mobile-rsync org-mobile-directory org-mobile-remote-path)))
  )

(use-package org-passwords
  :ensure t
  :init
  (setq org-passwords-file (concat (emacs-run-data-dir "org-mode" "passwords") "passwords.gpg")
        org-passwords-random-words-dictionary "/usr/share/dict/american-english")
  :commands (org-passwords-mode
             org-passwords
             org-passwords-generate-password
             org-passwords-generate-password-with-symbols)
  :bind (:map org-passwords-mode-map
              (("C-c u" . org-passwords-copy-username)
               ("C-c p" . org-passwords-copy-password)
               ("C-c o" . org-passwords-open-url)))
  )


(provide 'init-org-mode)
;;; init-org-mode.el ends here
