(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-echo-common ((t (:background "firebrick4" :foreground "pale turquoise"))))
 '(dired-async-message ((t (:foreground "dark olive grees"))))
 '(dired-async-mode-message ((t (:foreground "forest grees"))))
 '(diredp-compressed-file-suffix ((t (:foreground "DarkViolet"))))
 '(diredp-deletion ((t (:background "burlywood1" :foreground "Coral2"))))
 '(diredp-dir-name ((t (:background "gray90" :foreground "DarkRed"))))
 '(diredp-dir-priv ((t (:background "gray90" :foreground "DarkRed"))))
 '(diredp-flag-mark ((t (:background "DarkOrange4" :foreground "Yellow"))))
 '(ensime-implicit-highlight ((t (:foreground "DarkOrange2" :weight bold))))
 '(ensime-pending-breakpoint-face ((t (:underline (:color "peru" :style wave)))))
 '(ensime-writable-value-face ((t (:foreground "tan3" :weight bold))))
 '(helm-buffer-directory ((t (:foreground "dark green"))))
 '(helm-buffer-file ((t (:foreground "DeepSkyBlue4"))))
 '(helm-buffer-not-saved ((t (:foreground "Indianred2" :weight bold))))
 '(helm-buffer-process ((t (:foreground "DarkOrange2"))))
 '(helm-buffer-saved-out ((t (:foreground "red" :slant italic))))
 '(helm-buffer-size ((t (:foreground "gray10"))))
 '(helm-non-file-buffer ((t (:inherit italic :foreground "RosyBrown4"))))
 '(powerline-inactive1 ((t (:inherit powerline-active1 :background "grey70"))))
 '(powerline-inactive2 ((t (:inherit powerline-active2 :background "grey70")))))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-PDF-mode t)
 '(TeX-command-list
   (quote
    (("TeX" "%(PDF)%(tex) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (plain-tex-mode texinfo-mode ams-tex-mode)
      :help "Run plain TeX")
     ("LaTeX" "%`%l%(mode)%' %t" TeX-run-TeX nil
      (latex-mode doctex-mode)
      :help "Run LaTeX")
     ("Makeinfo" "makeinfo %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with Info output")
     ("Makeinfo HTML" "makeinfo --html %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with HTML output")
     ("AmSTeX" "%(PDF)amstex %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (ams-tex-mode)
      :help "Run AMSTeX")
     ("ConTeXt" "texexec --once --texutil %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt once")
     ("ConTeXt Full" "texexec %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt until completion")
     ("BibTeX" "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX")
     ("View" "xdg-open %s.pdf" TeX-run-discard t t :help "Run Viewer")
     ("Print" "%p" TeX-run-command t t :help "Print the file")
     ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command)
     ("File" "%(o?)dvips %d -o %f " TeX-run-command t t :help "Generate PostScript file")
     ("Index" "makeindex %s" TeX-run-command nil t :help "Create index file")
     ("Check" "lacheck %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for correctness")
     ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document")
     ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files")
     ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files")
     ("Other" "" TeX-run-command t t :help "Run an arbitrary command")
     ("Make" "make" TeX-run-command nil t)
     ("Make Clean" "make clean" TeX-run-shell nil t))))
 '(TeX-engine (quote xetex))
 '(TeX-engine-alist
   (quote
    ((makefile "Makefile build." "make TeX" "make LaTeX" "make ConTeXt"))))
 '(TeX-error-overview-open-after-TeX-run t)
 '(TeX-master (quote dwim))
 '(TeX-output-view-style
   (quote
    (("^dvi$"
      ("^landscape$" "^pstricks$\\|^pst-\\|^psfrag$")
      "%(o?)dvips -t landscape %d -o && gv %f")
     ("^dvi$" "^pstricks$\\|^pst-\\|^psfrag$" "%(o?)dvips %d -o && gv %f")
     ("^dvi$"
      ("^\\(?:a4\\(?:dutch\\|paper\\|wide\\)\\|sem-a4\\)$" "^landscape$")
      "%(o?)xdvi %dS -paper a4r -s 0 %d")
     ("^dvi$" "^\\(?:a4\\(?:dutch\\|paper\\|wide\\)\\|sem-a4\\)$" "%(o?)xdvi %dS -paper a4 %d")
     ("^dvi$"
      ("^\\(?:a5\\(?:comb\\|paper\\)\\)$" "^landscape$")
      "%(o?)xdvi %dS -paper a5r -s 0 %d")
     ("^dvi$" "^\\(?:a5\\(?:comb\\|paper\\)\\)$" "%(o?)xdvi %dS -paper a5 %d")
     ("^dvi$" "^b5paper$" "%(o?)xdvi %dS -paper b5 %d")
     ("^dvi$" "^letterpaper$" "%(o?)xdvi %dS -paper us %d")
     ("^dvi$" "^legalpaper$" "%(o?)xdvi %dS -paper legal %d")
     ("^dvi$" "^executivepaper$" "%(o?)xdvi %dS -paper 7.25x10.5in %d")
     ("^dvi$" "." "%(o?)xdvi %dS %d")
     ("^pdf$" "." "xpdf -remote %s -raise %o %(outpage)")
     ("^html?$" "." "netscape %o"))))
 '(TeX-view-program-list nil)
 '(ag-group-matches nil)
 '(ag-highlight-search t)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-for-comint-mode (quote filter))
 '(ansi-color-names-vector
   ["#2e3436" "#a40000" "#4e9a06" "#c4a000" "#204a87" "#5c3566" "#729fcf" "#eeeeec"])
 '(ansi-term-color-vector
   [unspecified "#FAFAFA" "#FF1744" "#66BB6A" "#F57F17" "#42A5F5" "#7E57C2" "#0097A7" "#546E7A"] t)
 '(auto-insert nil)
 '(auto-insert-directory "~/.emacs.d/auto-insert/")
 '(auto-insert-mode t)
 '(auto-save-interval 100)
 '(auto-save-visited-file-name nil)
 '(beacon-color "#F8BBD0")
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(bookmark-use-annotations t)
 '(bzr-work-offline t)
 '(c-default-style
   (quote
    ((c-mode . "linux")
     (c++-mode . "")
     (java-mode . "java")
     (idl-mode . "linux")
     (awk-mode . "awk")
     (other . "gnu"))))
 '(c-echo-syntactic-information-p t)
 '(c-report-syntactic-errors t)
 '(cfw:fchar-horizontal-line 9472)
 '(cfw:fchar-junction 9532)
 '(cfw:fchar-left-junction 9500)
 '(cfw:fchar-right-junction 9508)
 '(cfw:fchar-top-junction 9516)
 '(cfw:fchar-top-left-corner 9484)
 '(cfw:fchar-top-right-corner 9488)
 '(cfw:fchar-vertical-line 9474)
 '(cfw:fstring-period-end "]")
 '(cfw:fstring-period-start "[")
 '(cfw:org-capture-template
   (quote
    ("c" "calfw2org" entry
     (file nil)
     "* %?
    %(cfw:org-capture-day)")))
 '(column-number-mode t)
 '(company-idle-delay 0.75)
 '(company-show-numbers t)
 '(company-tooltip-idle-delay 1.5)
 '(company-transformers (quote (company-sort-by-backend-importance)))
 '(compilation-context-lines 3)
 '(compilation-environment nil)
 '(compilation-error-screen-columns nil)
 '(compilation-message-face (quote default))
 '(compilation-scroll-output t)
 '(completion-ignored-extensions
   (quote
    (".o" "~" ".bin" ".lbin" ".so" ".a" ".ln" ".blg" ".bbl" ".elc" ".lof" ".glo" ".idx" ".lot" ".svn/" ".hg/" ".git/" ".bzr/" "CVS/" "_darcs/" "_MTN/" ".fmt" ".tfm" ".class" ".fas" ".lib" ".mem" ".x86f" ".sparcf" ".fasl" ".ufsl" ".fsl" ".dxl" ".pfsl" ".dfsl" ".p64fsl" ".d64fsl" ".dx64fsl" ".lo" ".la" ".gmo" ".mo" ".toc" ".aux" ".cp" ".fn" ".ky" ".pg" ".tp" ".cps" ".fns" ".kys" ".pgs" ".tps" ".vrs" ".pyc" ".pyo" ".mod.c" ".mod.o" ".o.cmd" "~" "#" "dist/" "dist-newstyle/" "auto/")))
 '(conf-assignment-column 0)
 '(conf-colon-assignment-column 0)
 '(conf-colon-assignment-space t)
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#657b83")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-buffer-done-kill t)
 '(custom-buffer-style (quote brackets))
 '(custom-file "~/.emacs.d/custom/custom.el")
 '(custom-safe-themes
   (quote
    ("9749e084255ca616589a1d9d85067d707946e970bd1105e9f64899e984c40a2a" "1bf8855908bbbc3f6fd2383ce5473bf11594683eaa90a6ed0ff505f48d1adef1" "7c2b0de9a3f7f922646ad87734987b945698d7b3c8b3b18f34488653ffdbc8ec" "2b8346f431320ad3d3d0d995e3028d0b930b19c58edcfa2b96dfe735fc24409e" "2387d01981d4ae2c91d930265bdf981ea7730d645a183bb66d9b454a8458dc7a" "7ba6eb4eb1e2e7171e7daa3a7b5efabb2e3047353be054d6fe12f5b4c74ac66f" "d69a96b9db1048b7716f1a396bdb637b69232b882615eea82293128e2c1c11ea" "ff8ad6a87ffbad585dc5c5dad39e90554c90719314eb9228af64ab3d78b0bfa3" "375f75131a9a08e5dc23045b0c5002d7d02b4f92184f3192b7ffefd89f0e5b5a" "dc8841b277b95fed81562764c14938a7dccf78793c78ae2ca50681b5c96f95a1" "e745cc828e2ff0b7e7529bce641cfd30fdaf70e9ec3cbba64d11110e9014ddb5" "7fe047438a0eea7e929b1829fe6417f971dfcdf0e3eab327127a30f8fc517b9c" "99f51aec6305994ee7280bbb12fedf95f46b15e3920c0ce657d4d12e3e833a22" "69b81a4f7780e757fdd38e34fb928528c5f8161da055729192489b6bd96f96ba" "42b31159effb91f351c5f3d4bc26a88c535634de0e852138ebeea9529c9be651" "f871e5bd1a519c760d79ba1f53d4ed3dcdd98a11b0b1dbd31985d2850159e43e" "4346be3d1a1a00880646351974ee27cce98edecf1d855fa91c2311a4fd5cd7ec" "2333ac12b1d8641b21f555d1ed98011c9cc5283e72955e3eccdf81e2f16326be" "c854ef2e500f5c3b8e96d48ff067aa2915ef9458e43abccc9893d04fd55b1e3e" "08b0c65d71c9aedf461e16c57ff05cbd04f7fc2d138bfb194bb6af0eb507081f" "21a87301204afdc0adc906776323d6214ed77eaaf8e4a6e36a7c3deedcaa92f7" "585010e608a18de8605aa63f85b12d431639b7c68f465efc2c5d0925ee246e00" "6547d7fbc7bec01bec0c16c3a9a0dea01d8b74b0b062976b8dffe25f09f2e698" "e439a79c721a0ade8b6a8da0dae985c08cfbbd3adeb5b633076317589e09a0b6" "f788ea282c6c3f46383a806cd6c2bb857191a2e5a070af237ff2995c76f8a5b5" "3d69bc84e3be706b73f86ac49a8927d15f5d1715278f5f9c1d0d925cd4c27712" "63813036d671ee87f862e03e7ced55fc218821e7b48a0ba2466c7b75019c3c32" "932ada7608409a7aae56ac6ad86602577e7a42b0ba4dd872ffe1798d240e34fa" "a35e1f9a451aacd66f6dfadeaf8619f8b41f19eb15c0688ae38473447ea1cc33" "e5cb99ae91906068b5eccda68a648fc4d6c7b6d9669aff29375d5a8d5b557979" "f23070c5ccedb2d6f417831d88c5cb49fe8591c9887ec85a3b08ccd807bc7c9b" "768bae5c3a8075ecac2f2cc2c4dc4791200044f6ec9b6b34c8f3ccd6f34317a6" "bbe2d2e603ce2fa00caa09caee5ee36e2cdb9f7d97fb5c22a2cbccc840647edc" "7ee1c3dbc97338e615b6e081bf4ef01d3078808781ece8c40a056fa316c2f455" "a24c4b1c80568aa48b357ba4a12c57196ea52fd7d851d073f80906f9989b5980" "fb8ddaf0a8595f9d172dce1ee5e3ad2646c71d8c940260e140a2487f0b64aca5" "7de3342c672ba389ee4111b00522c4a8a3d361080146e280f0d484a0b8c36d93" "b4001a6b1a002d75f81ecd1f9608d0342cc502f752dbb08a2dadae9f1a1c3a53" "4e15f92be75b1f61ade0d8ab9f42a1547e7e2260d7dbe8838c9107ae3662f807" "df6597b9d6725426b852172b0880d85d2d5b716df548476dab559e3f132a3acf" "e414d929cc7f2481c6d547d19f15792084fb2a1ff042688e640998284821868d" "6e0b86fc8d4a3991a01a554b118c90a9c80fb2b9f3c9325fa986ab92bfbabbb6" "575fa80b959c350b210020a3254af23eb245ca64acc748032865a01e609db97d" "59da7b7397d279796c5581a9d6cd332368f22b9c3d7fcbc02e24b73de7b78553" "c9a39192e9f8003ad2608c954874cfb110c6948c7cb29d2b50d5921d5de32499" "42b2aad205e7b0d8df30a8ca743a962d566ffbf0addc72fd2cdfa56588680290" "82f9f7cc5b3061ad51dc54e130a2438d579191002f1ccbdae085cd09ed20ff39" "3a42aec74e7b5f3f8016f249dd3196c7e1fd1371c6cde8742868e547e54b0498" "905f9b20c31f729b20e2241f390e2d7491bc89b69611eae3b59c80e3a573e65d" "a5c5324b3d471825081fb8bbb7615d9b07ff70d7352003ae67d43695ac8b5092" "281f2aa411e42db1fa6495616b01efe4203cce3fee3718bfb822dd5e1733dc65" "1ae87cec63e91366b01c7baceeccec3171a7d797b6b3dd7621ff0c80821912a3" "ca56182cdba1da9f7853807617d67ddc195fdacb576d468ed88ecb84bd126214" "06de96a15fe0991463be081aa3c310e7085fa0597b6a29e80fe8360fb5da391f" "283101dc04d9b8af62699cc800eaa1930855ea6c1f87f9a69ea0e5c99c976e14" "740c9c4a3e01842c2bbd6fe67daa84623737e6c608151db46111bf41a412fa55" "292b7d71e7626df12b060e5c061a90e2a50cef629dc5d1f7aaebb4e6c2fcf29f" "a80b8445cdf8751fb3d01dfa0a6d9e2be57d4b7c9d511904133a819bf8139420" "6a446b4d2f33ae32e39c013622e7e0340cbda1279facbe262f4b101efa8b3cd6" "0e6aaffc267cf21e691fcaace3ba93b9885d203ddb131bd2e7e4dcb7bbdd04d6" "19967a73ae31b4e8263611ce8984ca7840d627605c12141e45c271b0a383967c" "287e433c4f7026a9453e72cb38794f39de50801bce0fa7ae671feab8b2729377" "e7003a21a2bb58f849a6e99dd32a55f653c70bcf90596e0df3079a842ba942ee" "4c1b71e34f680664a757b2edc87d4121ebe689779c8742fcf961279220b1b2ef" "0fbbf9ba7044d51e95e5dc5f282a7fefcba03a6c4520d1c8feb49f0e43276022" "db0257705281753260539f835ab6f70458c46523b3b610910ffe6b229feb91b6" "ac53ccd4977d3c2c11002adaba1cc0be35fabeae903e93f8234e50da66e7e93a" "395093e364e588a253edff2602a14842439c4a35fc84901a9508e74433010d51" "bf0610a5735c552b59de2ce8205f445e95f3054d6b00a17015a8ec9bba6992fa" "8f521b3c45789ef0c77410e74e47dba223c73127d3cce17ff91f4ba7d4840613" "ca434f6455652bdbfe46e6f6096cb5cfb13fc6aaf2367794b682ee333c787388" "78afadc55d5e46ea595daf5cc2d41374959b9044c7c7cf12c8b8aa3275c87464" "65a6ea20dfb5cb11cd639f2db7ce4da3fd655eeb08d855057e874a87f6d844e8" "1118ce7f17f20b1e3a4e264c201d13f06c5134e5c167ea74b191780ef24c9ed6" "321d7c8b180bd44a5b71eb091ead3fb32788ab7dc6814bbc31f91c5391d5ad0d" "6eddf2108401c63ac0d080971f5ec740145819ab787b9d127c792f48c9682c81" "fad3db92c719e88fa80a76acca0dd1dc99b2c6e7243b65c9b032e53ed841d537" "1f7bc1f47681a3cc9c0e6ba15774b80ddd2361a979f57a51bb84e7a44f4a98f0" "27eaa3ae50242b71342e6ebdaae93d5366438ec47e05438277f84e334965090e" "e31748149636e0295609a2d53b4a46a62ad9058f95bd7980abe7f8e1c30fb9c3" "1a0113389f17793058f7381c9ef001af517bf9cc0f6e1b8883438ff8dd0280d9" "4ea0adfc5b4380aea09e3604e428a05502773cd3c484c22b477b71e1eb356e67" "3cf6cf72868206b9dca78bfa4d85d27d14aee93696cfef2c4945eb3561044ce9" "8b1a56b4b2ee499c889a2352ac0bac54068743ce43b138fc75d86725b73f41b8" "4bd2401b74b8ff0c4af4ccf6a61eb6dde92e976afa4f2c7d59232188c120ce00" "45c75cb4e78ae2a8f0a9803bf0e3bfabd0770319ddddcc69cbefb30163fd6124" "039725d8488c8ea8690374e6e6e652ee1a84fa24c2b140a565e6e7ec9f965b98" "b1dbaa768077b8252e35646dacadec093a537613c79d2cac7ea95aeeccee0bc0" "6cdfb79a47d9b883d86da47af64ab3da46147c823e7be8408b136beca3e2e8c1" "cfd8b403df88179515c7c8316286ed8274825ce5d1bd8865c4cee4cbbf824846" "26c6fdc0ebf3781a56d7752284de8a88efcd211e5c8dae0adf21a46502f29cf4" "feafd98171985bce25d96d12a817e14f3313db4b062da8268390a520e51ffb2c" "56a2542dc1e2950f66aa6140d573edeb663078a63b4b49d12a098d7107bbf7ff" "ef59a493e29dbcb18850498250a572fc966fe6da93c8b1a8f1ff4a7e278d3f44" "ad9e485a07e81256068d72c824addef613e1b09448f9791478be479490878219" "287f8295295e5fa2e66dfd2b93985346bc2872a6f9faeca66c182f5c0a11753a" "8bb819577db1e6a4d1eb3e95cad4c2ea573edbed5c057fdc5c44d6e8d98d9e08" "e8825f26af32403c5ad8bc983f8610a4a4786eb55e3a363fa9acb48e0677fe7e" "08f214875fddc4a3cc516fa5d2b4a92ff2d1a904a04cf4cd60aa236b21e3329d" "56563bedf59579efa79d8cefd6678a455d9b1207e421e2ac4c25b5bd0e8d8336" "e6bc5e5aee18e77a819c8806393800bb91babd568101218f22b2744a74109887" "bf84b53e290da237733f9f6c376d217ef9ab1e6c9dd0cd2fe379b17e8f100ec1" "0b9d5471bbd74d807aedd9d2b1a0b803a4164019103a92640062c7e4fad52c53" "c0dd5017b9f1928f1f337110c2da10a20f76da0a5b14bb1fec0f243c4eb224d4" "bac3f5378bc938e96315059cd0488d6ef7a365bae73dac2ff6698960df90552d" "f831c1716ebc909abe3c851569a402782b01074e665a4c140e3e52214f7504a0" "ec0c9d1715065a594af90e19e596e737c7b2cdaa18eb1b71baf7ef696adbefb0" "595099e6f4a036d71de7e1512656e9375dd72cf60ff69a5f6d14f0171f1de9c1" "3a5f04a517096b08b08ef39db6d12bd55c04ed3d43b344cf8bd855bde6d3a1ae" "0b9f2c481b8bf1a5bc03a6756312c15d7128f0c668afc7abfebdf191c3f80f56" "8530b2f7b281ea6f263be265dd8c75b502ecd7a30b9a0f28fa9398739e833a35" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" "603a9c7f3ca3253cb68584cb26c408afcf4e674d7db86badcfe649dd3c538656" "40bc0ac47a9bd5b8db7304f8ef628d71e2798135935eb450483db0dbbfff8b11" "dba244449b15bdc6a3236f45cec7c2cb03de0f5cf5709a01158a278da86cb69b" "6cf0e8d082a890e94e4423fc9e222beefdbacee6210602524b7c84d207a5dfb5" "a455366c5cdacebd8adaa99d50e37430b0170326e7640a688e9d9ad406e2edfd" "5a0eee1070a4fc64268f008a4c7abfda32d912118e080e18c3c865ef864d1bea" "090bfe0fee0d6f5a63fcb60480c3c8ff08ed3391e0917fd462f6044d295c15a9" default)))
 '(custom-theme-directory "~/.emacs.d/themes/")
 '(diff-switches "-uN")
 '(dired-always-read-filesystem t)
 '(dired-auto-revert-buffer (quote dired-directory-changed-p))
 '(dired-kept-versions 100)
 '(dired-open-find-file-function (quote dired-find-file-other-window))
 '(dired-subtree-use-backgrounds nil)
 '(display-time-24hr-format t)
 '(display-time-day-and-date t)
 '(display-time-default-load-average nil)
 '(display-time-format "%FT%R%z")
 '(display-time-interval 15)
 '(display-time-mode t)
 '(electric-layout-mode t)
 '(electric-pair-mode t)
 '(evil-emacs-state-cursor (quote ("#D50000" bar)) t)
 '(evil-insert-state-cursor (quote ("#D50000" hbar)) t)
 '(evil-normal-state-cursor (quote ("#F57F17" box)) t)
 '(evil-visual-state-cursor (quote ("#66BB6A" box)) t)
 '(exec-path-from-shell-check-startup-files nil)
 '(explicit-shell-file-name "/usr/bin/zsh")
 '(fci-rule-color "#eee8d5")
 '(flymd-close-buffer-delete-temp-files t)
 '(flymd-output-directory "/tmp" t)
 '(fringe-mode 6 nil (fringe))
 '(gdb-many-windows t)
 '(global-font-lock-mode t)
 '(global-hi-lock-mode t)
 '(global-visual-line-mode t)
 '(global-whitespace-mode t)
 '(grep-find-ignored-directories
   (quote
    ("$RECYCLE.BIN" ".AppleDouble" ".AppleDB" ".DS_Store" ".DocumentRevisions-V100" ".LSOverride" ".Rhistory" ".Spotlight-V100" ".TemporaryItems" ".Trashes" ".actionScriptProperties" ".apdisk" ".apt_generated" ".bdfcache.el" ".build" ".buildpath" ".builds" ".bzr" ".cdv" ".classpath" ".com.apple.timemachine.donotpresent" ".com.apple.timemachine.supported" ".coverage" ".cproject" ".directory" ".dropbox" ".dropbox.cache" ".emacs-places" ".emacs.desktop" ".emacs.desktop.lock" ".eunit" ".externalToolBuilders" ".flexProperties" ".fseventsd" ".git" ".hg" ".idea" ".idlwave" ".ido.last" ".kkcrc" ".last_cover_stats" ".lein-deps-sum" ".loadpath" ".netrwhist" ".notes" ".org-id-locations" ".pc" ".project" ".projectile" ".prove" ".puppet-bak" ".quickurls" ".recentf" ".redcar" ".rspec" ".sass-cache" ".scala_dependencies" ".shadow_todo" ".shadows" ".strokes" ".svn" ".timelog" ".todo-do" ".todo-done" ".todo-top" ".tox" ".type-break" ".vip" ".viper" ".wmncach.el" ".yardoc" "_MTN" "__history" "_bdfcache.el" "_build" "_cgo_defun.c" "_cgo_gotypes.go" "_darcs" "_obj" "_sgbak" "_site" "_test" "_testmain.go" "_yardoc" "aclocal.m4" "auto-save-list" "autom4te.cache" "bin-debug" "bin-release" "blib" "build" "Build" "Build.bat" "COMMIT_EDITMSG" "cmake_install.cmake" "CMakeCache.txt" "CMakeFiles" "cover_db" "cscope.csd" "cscope.files" "cscope.inc" "cscope.lst" "cscope.out" "cscope.out.po" "cscope.tmplist" "CVS" "Debug" "debug" "depcomp" "DerivedData" "Desktop.ini" "ehthumbs.db" "GHI_ISSUE" "git-rebase-todo" "gwt-unitCache" "gwt_bree" "install-sh" "install_manifest.txt" "InstalledFiles" "Makefile.in" "Makefile.old" "MCVS" "META.yml" "MERGE_MSG" "minimal-session-saver-data.el" "MYMETA.yml" "nbbuild" "nbdist" "nosetests.xml" "nytprof" "nytprof.out" "perltidy.ERR" "pm_to_blib" "Profile" "profile" "RCS" "Release" "release" "SCCS" "Session.vim" "slprj" "SQUASH_MSG" "TAGS" "TAG_EDITMSG" "tags" "TestResult" "testresult" "Thumbs.db" "tmtags" "xcuserdata" "xhtml-loader.rnc" "{arch}" "~.dep" "~.dot" "~.nib" "~.plst" "test.out" "test_out" "test.output" "test_output" "cabal-sandbox" ".cabal-sandbox" "build" "dist")))
 '(grep-find-ignored-files
   (quote
    (".#*" "*.386" "*.a" "*.acn" "*.acr" "*.alg" "*.ap_" "*.apk" "*_archive" "*.asv" "*-autoloads.el" "*.aux" "*.bak" "*.bbl" "*.beam" "*.bin" "*.blg" "*.cgo1.go" "*.cgo2.c" "*.chi" "*.chi.h" "*.class" "*.com" "*.cp" "*.cps" "*.d64fsl" "*.dcu" "*.dep" "*.dex" "*.dfsl" "*.dll" "*.drc" "*.drv" "*.dvi" "*.dx32fsl" "*.dx64fsl" "*.dxl" "*.dylib" "*.ear" "*.elc" "*.esproj" "*-Ex.R" "*.exe" "*.fas" "*.fasl" "*.fdb_latexmk" "*.fmx" "*.fn" "*.fns" "*.fsl" "*.fx32fsl" "*.fx64fsl" "*.gcda" "*.gcno" "*.gcov" "*.glg" "*.glo" "*.gls" "*.gmo" "*.hi" "*.identcache" "*.ilg" "*.ilk" "*.iml" "*.ind" "*.ipr" "*.ist" "*.iws" "*.jar" "*.ky" "*.kys" "*.la" "*.lai" "*.launch" "*.lbin" "*.lib" "*.lnk" "*.lo" "*.lock" "*.lof" "*.lot" "*.lx32fsl" "*.lx64fsl" "*.maf" "*.mem" "*.min.js" "*-min.js" "*.mmx" "*.mo" "*.moved-aside" "*.mtc" "*.mtc0" "*.nav" "*.nlo" "*.o" "*.obj" "*.opensdf" "*.orig" "*.p64fsl" "*.pdfsync" "*.pfsl" "*.pg" "*.pgs" "*.pid" "*.pidb" "*.plt" "*.plx" "*.pot" "*.psess" "*.Publish.xml" "*.pyc" "*.pyd" "*.pydevproject" "*.pyo" "*.rbc" "*.rej" "*.sassc" "*.scc" "*.sdf" "*.seed" "*.sln.docstates" "*.slo" "*.snm" "*.so" "*.sparcf" "*.sublime-project" "*.sublime-workspace" "*.suo" "*.swo" "*.swp" "*.sx32fsl" "*.sx64fsl" "*.synctex.gz" "*.ttc" "*.tfm" "*.tmproj" "*.tmproject" "*.toc" "*.tp" "*.tps" "*.ufsl" "*.un~" "*.vr" "*.vrb" "*.vrs" "*.vsp" "*.vspscc" "*.vssscc" "*.vxd" "*.war" "*.wx32fsl" "*.wx64fsl" "*.x86f" "*.xdy" "*.zwc" "*~" "cabal.sandbox.config")))
 '(helm-ff-candidate-number-limit 500)
 '(helm-find-files-actions
   (quote
    (("Find file" . find-file)
     ("Find file in Dired" . helm-point-file-in-dired)
     ("View file" . view-file)
     ("Query replace fnames on marked `M-%'" . helm-ff-query-replace-on-marked)
     ("Marked files in dired" . helm-marked-files-in-dired)
     ("Query replace contents on marked" . helm-ff-query-replace)
     ("Query replace regexp contents on marked" . helm-ff-query-replace-regexp)
     ("Serial rename files" . helm-ff-serial-rename)
     ("Serial rename by symlinking files" . helm-ff-serial-rename-by-symlink)
     ("Serial rename by copying files" . helm-ff-serial-rename-by-copying)
     ("Open file with default tool" . helm-open-file-with-default-tool)
     ("Find file in hex dump" . hexl-find-file)
     ("Browse project" . helm-ff-browse-project)
     ("Complete at point `C-c i'" . helm-insert-file-name-completion-at-point)
     ("Insert as org link `C-c @'" . helm-files-insert-as-org-link)
     ("Find shell command `C-c /'" . helm-ff-find-sh-command)
     ("Add marked files to file-cache" . helm-ff-cache-add-file)
     ("Open file externally `C-c C-x, C-u to choose'" . helm-open-file-externally)
     ("Grep File(s) `C-s, C-u Recurse'" . helm-find-files-grep)
     ("Grep current directory with AG" . helm-find-files-ag)
     ("Git grep" . helm-ff-git-grep)
     ("Zgrep File(s) `M-g z, C-u Recurse'" . helm-ff-zgrep)
     ("Gid" . helm-ff-gid)
     ("Switch to Eshell `M-e'" . helm-ff-switch-to-eshell)
     ("Etags `M-., C-u reload tag file'" . helm-ff-etags-select)
     ("Eshell command on file(s) `M-!, C-u take all marked as arguments.'" . helm-find-files-eshell-command-on-file)
     ("Find file as root `C-c r'" . helm-find-file-as-root)
     ("Find alternate file" . find-alternate-file)
     ("Ediff File `C-c ='" . helm-find-files-ediff-files)
     ("Ediff Merge File `M-='" . helm-find-files-ediff-merge-files)
     ("Delete File(s) `M-D'" . helm-delete-marked-files)
     ("Copy file(s) `M-C, C-u to follow'" . helm-find-files-copy)
     ("Rename file(s) `M-R, C-u to follow'" . helm-find-files-rename)
     ("Backup files" . helm-find-files-backup)
     ("Symlink files(s) `M-S, C-u to follow'" . helm-find-files-symlink)
     ("Relsymlink file(s) `C-u to follow'" . helm-find-files-relsymlink)
     ("Hardlink file(s) `M-H, C-u to follow'" . helm-find-files-hardlink)
     ("Find file other window `C-c o'" . helm-find-files-other-window)
     ("Switch to history `M-p'" . helm-find-files-switch-to-hist)
     ("Print File `C-c p, C-u to refresh'" . helm-ff-print)
     ("Locate `C-x C-f, C-u to specify locate db'" . helm-ff-locate)
     ("ZTree Directory" . helm-open-ztree))))
 '(helm-fuzzier-max-query-len 15)
 '(helm-fuzzier-preferred-max-group-length 5)
 '(helm-grep-actions
   (quote
    (("Find file" . helm-grep-other-window)
     ("Save results in grep buffer" . helm-grep-save-results))))
 '(helm-source-multi-occur-actions (quote (("Goto line" . helm-moccur-goto-line-of))))
 '(helm-type-buffer-actions
   (quote
    (("Switch to buffer `C-c C-o'" . switch-to-buffer)
     ("Browse project from buffer" . helm-buffers-browse-project)
     ("Query replace regexp `C-M-%'" . helm-buffer-query-replace-regexp)
     ("Query replace `M-%'" . helm-buffer-query-replace)
     ("View buffer" . view-buffer)
     ("Display buffer" . display-buffer)
     ("Grep buffers `M-g s' (C-u grep all buffers)" . helm-zgrep-buffers)
     ("Multi occur buffer(s) `C-s'" . helm-multi-occur-as-action)
     ("Revert buffer(s) `M-U'" . helm-revert-marked-buffers)
     ("Insert buffer" . insert-buffer)
     ("Kill buffer(s) `M-D'" . helm-kill-marked-buffers)
     ("Diff with file `C-='" . diff-buffer-with-file)
     ("Ediff Marked buffers `C-c ='" . helm-ediff-marked-buffers)
     ("Ediff Merge marked buffers `M-='" .
      #[257 "\300\301\"\207"
            [helm-ediff-marked-buffers t]
            4 "

(fn CANDIDATE)"]))))
 '(helm-type-file-actions
(quote
 (("Find file" . helm-find-many-files)
  ("Find file as root" . helm-find-file-as-root)
  ("Find file other window" . helm-find-files-other-window)
  ("Open dired in file's directory" . helm-open-dired)
  ("Marked files in dired" . helm-marked-files-in-dired)
  ("Grep File(s) `C-u recurse'" . helm-find-files-grep)
  ("Zgrep File(s) `C-u Recurse'" . helm-ff-zgrep)
  ("Pdfgrep File(s)" . helm-ff-pdfgrep)
  ("Insert as org link" . helm-files-insert-as-org-link)
  ("Checksum File" . helm-ff-checksum)
  ("Ediff File" . helm-find-files-ediff-files)
  ("Ediff Merge File" . helm-find-files-ediff-merge-files)
  ("Etags `M-., C-u reload tag file'" . helm-ff-etags-select)
  ("View file" . view-file)
  ("Insert file" . insert-file)
  ("Add marked files to file-cache" . helm-ff-cache-add-file)
  ("Delete file(s)" . helm-delete-marked-files)
  ("Copy file(s) `M-C, C-u to follow'" . helm-find-files-copy)
  ("Rename file(s) `M-R, C-u to follow'" . helm-find-files-rename)
  ("Symlink files(s) `M-S, C-u to follow'" . helm-find-files-symlink)
  ("Relsymlink file(s) `C-u to follow'" . helm-find-files-relsymlink)
  ("Hardlink file(s) `M-H, C-u to follow'" . helm-find-files-hardlink)
  ("Open file externally (C-u to choose)" . helm-open-file-externally)
  ("Open file with default tool" . helm-open-file-with-default-tool)
  ("Find file in hex dump" . hexl-find-file))))
 '(hi-lock-auto-select-face t)
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-nonselected-windows t)
'(highlight-symbol-colors
(quote
 ("#F57F17" "#66BB6A" "#0097A7" "#42A5F5" "#7E57C2" "#D84315")))
 '(highlight-symbol-foreground-color "#546E7A")
 '(highlight-tail-colors (quote (("#F8BBD0" . 0) ("#FAFAFA" . 100))))
'(hl-bg-colors
(quote
 ("#DEB542" "#F2804F" "#FF6E64" "#F771AC" "#9EA0E5" "#69B7F0" "#69CABF" "#B4C342")))
'(hl-fg-colors
(quote
 ("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3")))
'(hl-paren-colors
(quote
 ("#B9F" "#B8D" "#B7B" "#B69" "#B57" "#B45" "#B33" "#B11")))
 '(horizontal-scroll-bar-mode nil)
 '(imenu-auto-rescan t)
 '(imenu-sort-function (quote imenu--sort-by-name))
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries (quote left))
 '(indicate-empty-lines t)
'(insert-shebang-ignore-extensions
(quote
 ("txt" "org" "el" "hs" "fs" "cs" "c" "h" "cpp" "cxx" "hpp" "hxx" "Makefile" "makefile" "mk")))
 '(jdee-server-dir "/home/evan/.emacs.d/tum.d/")
 '(jit-lock-chunk-size 2500)
 '(jit-lock-context-time 0.25)
 '(jit-lock-stealth-load 5)
 '(jit-lock-stealth-nice 0.25)
 '(js2-allow-member-expr-as-function-name t)
 '(js2-bounce-indent-p t)
 '(js2-highlight-level 3)
 '(json-reformat:indent-width 2)
 '(json-reformat:pretty-string\? t)
 '(linum-format (quote dynamic))
 '(list-colors-sort (quote (hsv-dist . "black")))
 '(mail-host-address "theunixman.com")
'(mode-line-format
(quote
 ("%e" mode-line-front-space mode-line-mule-info mode-line-client mode-line-modified mode-line-remote mode-line-frame-identification mode-line-buffer-identification "   " mode-line-position "  " mode-line-modes mode-line-misc-info mode-line-end-spaces)))
 '(mouse-wheel-follow-mouse t)
 '(neo-auto-indent-point t)
 '(neo-banner-message "Current Tree")
 '(neo-create-file-auto-open t)
 '(neo-cwd-line-style (quote button))
 '(neo-dont-be-alone t)
'(neo-hidden-regexp-list
(quote
 ("^\\." "\\.pyc$" "~$" "^#.*#$" "\\.elc$" "cabal.sandbox.config" "^dist/" "^session\\.")))
 '(neo-smart-open t)
 '(neo-theme (quote arrow))
 '(neo-window-position (quote right))
 '(neo-window-width 45)
 '(next-error-highlight 5)
 '(next-error-highlight-no-select 5)
 '(next-error-recenter (quote (4)))
'(nrepl-message-colors
(quote
 ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-agenda-columns-add-appointments-to-effort-sum t)
'(org-agenda-files
(quote
 ("/projects/theunixman/vojtech.org" "/projects/theunixman/image-mining/anomaly-detection/doc/Project.org" "/projects/misandrist/Eviction/TODO.org" "/projects/theunixman/rossignol/MODELS.org" "/projects/theunixman/visual.org" "/projects/nielsen/PDM Generation/TODO.org" "/projects/misandrist/usrrname/TODO.org" "/projects/nielsen/Repositories/BNDXT-1431.org" "/projects/nielsen/Issues/BNDXT-1341/TODO.org" "/projects/theunixman/nielsen/TODO.org" "/projects/misandrist/AstralAR/IRS/Misclassification/TODO.org" "/projects/Experfy.org" "/projects/theunixman/Guru/LinuxAuditd/PROJECT.org" "/projects/theunixman/Joerg/TODO.org" "~/INTERVIEWS.org" "/projects/theunixman/Experfy/HIPAA/TODO.org" "/projects/theunixman/Guru/LSM-VFS/TODO.org" "/projects/seam/NOTES.org" "/projects/theunixman/Guru/TODO.org" "/projects/doctors/TODO.org" "/projects/misandrist/marquis/NOTES.org" "/projects/misandrist/ranga/NOTES.org" "/projects/realtimelogic/TODO.org" "/projects/mindview/TODO.org" "/projects/navipoint/navipoint-webapp/TODO.org" "/projects/navipoint/NOTES/TODO.org" "~/Documents/Excluded/TODO.org.gpg")))
 '(org-agenda-follow-indirect t)
 '(org-agenda-include-diary t)
 '(org-agenda-insert-diary-extract-time t)
 '(org-agenda-persistent-filter t)
 '(org-agenda-persistent-marks t)
 '(org-agenda-restore-windows-after-quit t)
 '(org-agenda-skip-deadline-if-done t)
 '(org-agenda-skip-unavailable-files t)
 '(org-agenda-start-with-follow-mode nil)
'(org-agenda-time-grid
(quote
 ((daily weekly today require-timed)
  (800 1000 1200 1400 1600 1800 2000)
  "......" "----------------")))
 '(org-agenda-time-leading-zero t)
 '(org-agenda-use-time-grid t)
 '(org-agenda-view-columns-initially t)
 '(org-blank-before-new-entry (quote ((heading . t) (plain-list-item . auto))))
'(org-capture-templates
(quote
 (("\"bo\"" "* ONLINE: %:btype" entry
   (file "references.org")
   "%:author (%:year): %:title"))))
 '(org-columns-ellipses "...")
 '(org-ctrl-k-protect-subtree (quote error))
 '(org-footnote-auto-adjust t)
 '(org-footnote-define-inline t)
 '(org-footnote-fill-after-inline-note-extraction t)
 '(org-footnote-section nil)
 '(org-inlinetask-show-first-star t)
 '(org-insert-heading-respect-content t)
 '(org-list-allow-alphabetical t)
 '(org-list-empty-line-terminates-plain-lists t)
 '(org-list-indent-offset 2)
 '(org-list-use-circular-motion t)
 '(org-mobile-directory "/home/evan/.emacs.d/run/org/mobile/" t)
 '(org-mobile-encryption-password "foob9aigha7ahghaix,a")
'(org-mobile-files
(quote
 (org-agenda-files org-agenda-text-search-extra-files)))
 '(org-mobile-files-exclude-regexp ".*\\.gpg")
 '(org-mobile-inbox-for-pull "/home/evan/.emacs.d/run/org/inbox/" t)
 '(org-mobile-use-encryption t)
'(org-modules
(quote
 (org-bbdb org-bibtex org-crypt org-ctags org-docview org-gnus org-info org-inlinetask org-irc org-protocol org-rmail org-w3m org-bookmark org-checklist org-choose org-collector org-elisp-symbol org-eshell org-expiry org-index org-git-link org-man org-registry)))
 '(org-show-entry-below (quote ((default) (agenda . t) (org-goto . t))))
 '(org-show-following-heading (quote ((default . t))))
 '(org-startup-indented t)
 '(org-use-property-inheritance t)
 '(package-check-signature (quote allow-unsigned))
'(package-directory-list
(quote
 ("/usr/local/share/emacs/24.5/site-lisp/elpa" "/usr/local/share/emacs/site-lisp/elpa" "/usr/share/emacs/24.5/site-lisp/elpa" "/usr/share/emacs/site-lisp/elpa" "~/.emacs.d/tum/packages")))
'(package-selected-packages
(quote
 (auto-yasnippet bbdb-csv-import company-ansible company-arduino company-axiom dotenv-mode ein helm-pydoc helm-rage nlinum markdown-mode helm-bibtex flyspell-correct irony use-package helm-flycheck helm-flymake helm-flyspell helm-system-packages helm-systemd helm-tramp helm-xref clang-format jdee lsp-javacomp meghanada tern tern-auto-complete tern-context-coloring org-passwords annotate annotate-depth eslintd-fix import-js indent-info indent-tools indium inline-crypt insert-shebang ipython-shell-send iter2 jira-markup-mode jpop jq-mode js-codemod js-comint keyfreq keyword-search latexdiff lcr lentic-server less-css-mode letcheck linum-relative liquid-types lisp-extra-font-lock live-py-mode lively livereload load-relative loc-changes lognav-mode logstash-conf logview look-dired lsp-javascript-typescript lsp-ocaml lsp-ui magit-annex magit-gitflow magit-stgit mc-extras md-readme messages-are-flowing mhc mini-header-line morlock msvc nameframe nameframe-projectile nash-mode nasm-mode neato-graph-bar network-watch niceify-info ninja-mode noccur nocomments-mode nodejs-repl number numbers nv-delete-back nvm ob-cfengine3 olivetti on-screen outshine owdriver ox-bibtex-chinese ox-epub package-build paradox paren-face pcre2el peep-dired peg persistent-overlays persp-fr persp-mode postcss-sorting powershell preproc-font-lock ob-async ob-blockdiag ob-browser ob-clojure-literate ob-php ob-spice ob-translate ob-typescript ob-uart org org-agenda-property org-alert org-brain org-caldav org-category-capture org-clock-convenience org-commentary org-edna org-gcal org-grep org-iv org-jira org-link-minor-mode org-mobile-sync org-noter org-pdfview org-recent-headings org-sticky-header org-super-agenda org-sync-snippets org-table-sticky-header org-web-tools org-wild-notifier org2elcomment ox-asciidoc ox-clip ox-jira ox-pandoc ox-reveal ox-rst ox-textile ox-trac ox-tufte ox-twbs secretaria yankpad ebib company-childframe company-php company-terraform skewer-less skewer-mode skewer-reload-stylesheets smart-jump smart-semicolon smart-tabs-mode smooth-scrolling solidity-mode spaceline spaceline-all-the-icons spinner splitter tabbar tabbar-ruler terraform-mode texfrag toc-org typescript-mode company-coq company-glsl company-lsp company-web ob-diagrams ob-fsharp ob-http ob-ipython ob-prolog ob-restclient ob-sml ob-sql-mode orca org-attach-screenshot org-babel-eval-in-repl org-beautify-theme org-board org-capture-pop-frame org-dp org-easy-img-insert org-edit-latex org-ehtml org-journal org-linkany org-mime org-mru-clock org-parser org-pomodoro org-review org-seek org-wc orgtbl-aggregate outorg ox-gfm ox-minutes packed page-break-lines paredit perspeen pomidor popup-switcher powerline prettier-js pretty-mode projectile-speedbar prompt-text prompts purescript-mode quelpa-use-package realgud-old-debuggers request-deferred sauron schrute tum-frames helm-projectile company-restclient dired-rainbow direnv direx ensime focus json-reformat jsonnet-mode judge-indent mustache restclient systemd term-manager total-lines tracking typo underline-with-char web-beautify web-completion-data webpaste xml-rpc flycheck-demjsonlint flycheck-lilypond flycheck-liquidhs flycheck-pycheckers folding fsharp-mode function-args ggtags xterm-frobs xterm-keybinder xterm-title xwidgete yasnippet-snippets ac-clang apt-sources-list auto-indent-mode js2-refactor helm-swoop org-mobile org-dashboard org-bullets bind-chord bind-key use-package-ensure-system-package csharp-mode csound-mode graphql-mode ids-edit org-mind-map org-notebook org-password-manager org-projectile org-projectile-helm org-repo-todo orgit orglue outline-toc outrespace pandoc-mode parsec proportional sotlisp sourcemap spice-mode ssass-mode super-save sync-recentf treemacs-evil wgrep-ag ten-hundred-mode company-clang typopunct diminish diredfl ac-php ac-php-core composer flymake-php flymake-phpcs phan php+-mode php-auto-yasnippets php-boris php-boris-minor-mode php-cs-fixer php-refactor-mode php-runtime psysh purescript-decl-scan-mode org-doing org-context org-cliplink org-bookmark-heading org-autolist org-ref org-download org-ac calfw-org psc-ide-mode addressbook-bookmark psc-ide psci real-auto-save recover-buffers related relative-buffers replace-with-inflections restart-emacs rigid-tabs runner scad-mode scad-preview scratch search-web selected shell-pop shift-number shift-text shimbun show-css smart-compile smart-tab snapshot-timemachine snapshot-timemachine-rsnapshot springboard ssh-agency symbol-overlay syntactic-close telephone-line thingopt thinks tiny todotxt togetherly tramp-hdfs unicode-emoticons unicode-enbox unidecode unipoint vdirel wc-mode web-mode-edit-element web-search window-jump window-layout window-number window-purpose windwow winnow wordgen www-synonyms x86-lookup yarn-mode yatemplate zlc zpresent flycheck-jslint helm-jstack jdecomp jtags lsp-java thread-dump java-imports java-snippets javadoc-lookup maven-test-mode mvn wakatime-mode wakatime sql-impala sr-speedbar helm-c-yasnippet linkd preseed-generic-mode company-ghci helm-company manual helm-cscope helm-flx helm-ag helm-fuzzier helm-themes helm-package helm-descbinds helm-gitignore helm-proc company-ghc add-node-modules-path auto-save-buffers-enhanced biblio biblio-core bison-mode buffer-watcher cframe comment-tags cosmo dim dim-autoload dired-collapse dired-subtree easy-after-load flycheck-flawfinder gams-mode github-theme gitpatch gnus-select-account gscholar-bibtex bog autovirtualenv ac-html-angular ac-rtags angular-mode angular-snippets auto-async-byte-compile bash-completion beacon bifocal company-emacs-eclim dired-sidebar ez-query-replace flycheck-ycmd font-lock-profiler js-auto-beautify kanban keymap-utils know-your-http-well list-unicode-display loccur magic-filetype magithub ng2-mode nlinum-hl noaa occur-context-resize projectile-ripgrep term-cmd thing-cmds biblio-crossref lsp-haskell-mode lsp-flycheck lsp-python-mode xpm cmake-ide-mode lentic bibtex-completion web-narrow-mode whole-line-or-region wsd-mode wttrin x509-mode zoutline flycheck-rtags grab-x-link grass-mode hlint-refactor ivariants ivs-edit jabber-otr kill-or-bury-alive kurecolor lacarte lib-requires lice lispxmp literal-string major-mode-icons markdown-preview-mode mentor mips-mode mmt mode-icons modeline-char modeline-posn move-dup move-text multi-line multi-term nginx-mode opencl-mode palimpsest pcap-mode persistent-scratch persp-projectile riscv-mode scheme-complete sift subemacs suggest suggestion-box synonyms better-shell bibslurp bm boogie-friends boxquote bpr browse-at-remote char-menu click-mode cloc cyphejor default-text-scale dtrt-indent elisp-depend emacsql-sqlite autobookmarks bbdb bshell company-irony face-explorer finder+ flimenu flx-isearch flycheck-cstyle flycheck-irony flycheck-pos-tip helm-cider highlight-chars highlight-numbers ibuffer-projectile isend-mode json-navigator jump-tree keychain-environment llvm-mode lockfile-mode macros+ magic-latex-buffer magit-imerge magit-lfs magit-tbdiff markdown-edit-indirect math-symbols mb-depth+ mic-paren misc-cmds misc-fns mixed-pitch navi-mode org-capture ac-emacs-eclim ace-jump-buffer ace-link add-hooks aggressive-indent all-the-icons-gnus all-the-icons-ivy apiwrap orgnav package+ palette paperless paredit-everywhere parinfer pcache persp-mode-projectile-bridge phi-autopair phi-grep phi-search phi-search-migemo po-mode point-pos point-stack poporg popup-edit-menu popup-imenu pov-mode pp+ projectile-git-autofetch projectile-variable psession quickref quickrun recentf-ext rectangle-utils refine regex-tool replace+ replace-from-region replace-pairs replace-symbol rich-minority ripgrep robots-txt-mode sane-term sass-mode save-visited-files sentence-highlight sequences simple-bookmarks simple-call-tree simpleclip skeletor slime-company slirm smart-comment smart-hungry-delete smart-mode-line smartscan spaces speech-tagger sql-indent sqlup-mode srefactor stem-english string-inflection strings subr+ switch-window swoop synosaurus tagedit terminal-here test-c thingatpt+ time-ext tool-bar+ treepy unfill unify-opening vdiff vdiff-magit viewer visual-fill-column visual-regexp visual-regexp-steroids wgrep-helm whitespace-cleanup-mode wid-edit+ x-path-walker xcscope zones auto-virtualenv autopair pyenv-mode pyenv-mode-auto lsp-haskell lsp-mode lsp-python pip-requirements pippel abl-mode anaconda-mode auto-virtualenvwrapper cinspect flycheck-pyflakes jedi-direx pungi py-autopep8 py-isort py-smart-operator py-test py-yapf pycoverage pygen pyimport pylint pytest python-environment python-switch-quotes sphinx-doc sphinx-frontend sphinx-mode tox yapfify jedi jedi-core js-doc js-format js-import code-library codesearch col-highlight comment-dwim-2 commify common-lisp-snippets company-anaconda company-jedi company-lua composable copy-as-format cov cryptol-mode csv cython-mode dante dashboard datetime desktop+ dic-lookup-w3m dictcc dired-explorer dired-fdclone dired-hide-dotfiles dired-icon dired-launch dired-single diredful dotnet download-region dpaste dtrace-script-mode dummy-h-mode dynamic-ruler ebal ecb edbi edbi-database-url edbi-minor-mode edit-server eide eimp el2markdown el2org elisp-docstring-mode elisp-format elisp-lint elmacro elog elpa-clone elpa-mirror elquery elx emacsql emaps embrace emlib emmet-mode emojify empos emr engine-mode epkg ereader esh-buf-stack esh-help eshell-fringe-status eshell-git-prompt eshell-up eshell-z esup esxml evalator exiftool exsqlaim-mode f3 face-remap+ fcitx files+ filesets+ find-file-in-project find-file-in-repository flex-isearch flycheck-checkbashisms flycheck-clang-analyzer flycheck-clang-tidy flycheck-clangcheck flycheck-color-mode-line flycheck-cython flycheck-inline flycheck-mypy flycheck-popup-tip flymake-cppcheck flymake-less flyspell-correct-ivy flyspell-correct-popup font-lock-studio fuff fuzzy-match fxrd-mode ghub github-pullrequest gitlab glsl-mode gnuplot-mode grails-projectile-mode graphviz-dot-mode guess-language gulp-task-runner gxref helm-bibtexkey helm-codesearch helm-google helm-open-github hexrgb hide-comnt hideshowvis hierarchy highlight highlight-context-line highlight-current-line highlight-function-calls highlight-indent-guides highlight-operators highlight-parentheses highlight-refontification highlight-stages highlight-symbol highlight-thing himp historian hl-defined hl-indent hl-line+ hl-spotlight hl-todo hlinum id-manager immortal-scratch import-popwin importmagic indicators info-buffer inherit-local ini-mode inline-docs interleave isearch-prop isortify itail iterator ivy-dired-history ivy-historian ivy-pages ivy-todo calfw-gcal enlive flycheck-scala-sbt maker-mode scala-mode demangle-mode disaster flymake-haskell-multi flymake-hlint flymake-jslint flymake-json flymake-python-pyflakes flyspell-popup qt-pro-mode browse-kill-ring browse-kill-ring+ buffer-manage buffer-sets c-eldoc calfw calfw-cal calfw-ical cff cg cil-mode cmake-project cmds-menu color-identifiers-mode comint-intercept company-rtags concurrent coq-commenter ctags-update ag all-the-icons all-the-icons-dired apropos-fn+var async-await auctex-lua auto-minor-mode autothemer bibretrieve bibtex-utils bookmark+ cmake-font-lock cmake-ide cmake-mode flycheck-tip flycheck-yamllint flyspell-correct-helm promise protobuf-mode px pydoc python-docstring modern-cpp-font-lock tum-haskell-unicode-input-method apu lua-mode flymake-lua irony-eldoc ace-isearch ace-flyspell dired-sort-menu+ dired-quicksort edit-indirect-region-latex edit-indirect dired-async irfc async-bytecomp smart-quotes ac-ctags flymd fstar-mode mustache-mode debian-changelog-mode debian-changelog proof pgdevenv pg restclient-helm parsebib ldap-mode asn1-mode requirejs company-dict company-bibtex apache-mode indent-guide ztree org-plus-contrib use-package-chords math-symbol-lists markdownfmt latex-unicode-math-mode latex-unicode-math latex-preview-pane latex-pretty-symbol latex-math-preview latex-extra history hcl-mode helm-fussier ghc magit-submodule helm-bookmark psc-ide-flycheck magit-subtree helm-adaptive-mode magit-wip helm-info helm-mode haskell-mode company-quickhelp company-dabbrev company async helm-pages dired+ dired-atool dired-filetype-face dired-filter dired-imenu dired-k dired-narrow dired-quick-sort dired-ranger zonokai-theme zenburn-theme zen-and-art-theme z3-mode yaml-mode xterm-color xref-js2 xquery-tool xquery-mode xmlunicode xmlgen xml+ window+ white-sand-theme wcheck-mode w3 validate unicode-whitespace unicode-troll-stopper unicode-progress-reporter unicode-input unicode-fonts unicode-escape uni-confusables undohist undo-tree ujelly-theme ucs-cmds tommyh-theme tidy thumb-frm termbright-theme term-projectile term-alert term+ tao-theme syntax-subword suscolors-theme sunny-day-theme subatomic-theme strace-mode stekene-theme state ssh-config-mode spike-theme solarized-theme soft-stone-theme soft-morning-theme slime-theme simple+ silkworm-theme shm sed-mode scss-mode scion req-package redo+ quasi-monochrome-theme projectile-direnv polymode pinentry perspective pastelmac-theme password-vault parse-csv paren-completer paredit-menu paper-theme pandoc package-utils pabbrev otama origami orgtbl-ascii-plot orglink org2issue org-tree-slide org-transform-tree-table org-tracktable oneonone oldlace-theme oauth2 npm-mode noxml-fold nlinum-relative nhexl-mode neotree narrowed-page-navigation narrow-indirect name-this-color muttrc-mode multishell multicolumn mouse+ monokai-theme moe-theme minimap menu-bar+ memory-usage mbo70s-theme markdown-toc markdown-mode+ majapahit-theme magit-tramp magit-svn magit-gh-pulls magit-find-file magit-filenotify load-dir light-soap-theme lenlen-theme lavender-theme kite-mini json-rpc json-mode js2-highlight-vars js2-closure jg-quicknav jazz-theme jabber ivy-hydra ivy-gitlab ivy-bibtex isearch+ iodine-theme imenus imenu-list imenu-anywhere imenu+ image-dired+ ignoramus ido-vertical-mode ido-ubiquitous ido-occur ido-hacks ido-grid-mode icomplete+ icicles hydandata-light-theme hyai html5-schema help-mode+ help-fns+ help+ helm-org-rifle helm-mt helm-mode-manager helm-ispell helm-helm-commands helm-grepint helm-git-files helm-git helm-fuzzy-find helm-filesets helm-dirset helm-dired-recent-dirs helm-dictionary helm-describe-modes helm-css-scss header2 haskell-tab-indent haskell-snippets grizzl greymatters-theme grep+ green-phosphor-theme goto-last-change glab github-search github-notifier github-issues github-clone github-browse-file gitconfig-mode git-timemachine git-messenger git-lens git-gutter-fringe git-gutter-fringe+ genrnc general fuzzy furl fraktur-mode format-sql form-feed fontawesome font-lock+ flyparens flymake-yaml flymake-shell flymake-sass flymake-gjshint flymake-css flylisp flycheck-status-emoji flycheck-purescript flycheck-pkg-config flycheck-package flycheck-hdevtools flycheck-haskell flycheck-css-colorguard flatui-theme find-dired+ farmhouse-theme faff-theme faces+ facemenu+ exec-path-from-shell espresso-theme eslint-fix emamux emacsql-psql emacsql-mysql docbook dired-open dired-details+ diff-hl cus-edit+ cssfmt css-eldoc css-comb crux crontab-mode cpputils-cmake context-coloring config-parser company-statistics company-shell company-qml company-ngram company-math company-flx company-cabal company-c-headers company-auctex colorsarenice-theme color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized color-theme-modern color-theme-buffer-local color-theme-approximate cider-spy cider-profile cider-eval-sexp-fu cider-decompile cherry-blossom-theme caroline-theme bubbleberry-theme badwolf-theme autumn-light-theme autodisass-llvm-bitcode auto-overlays auto-complete-rst auto-complete-nxml auto-complete-exuberant-ctags auto-complete-c-headers auto-complete-auctex auctex-latexmk ascii-art-to-unicode arjen-grey-theme apropospriate-theme amd-mode all-ext aggressive-fill-paragraph adaptive-wrap ace-jump-helm-line ac-math ac-js2 ac-ispell ac-html-csswatcher ac-html-bootstrap ac-html ac-helm ac-haskell-process ac-etags)))
 '(package-unsigned-archives nil)
 '(pos-tip-background-color "#ffffff")
 '(pos-tip-foreground-color "#78909C")
 '(powerline-gui-use-vcs-glyph t)
 '(preview-auto-cache-preamble nil)
'(preview-dump-replacements
(quote
 (preview-LaTeX-command-replacements
  ("\\`\\([^ ]+\\)\\(\\( +-\\([^ \\\\\"]\\|\\\\\\.\\|\"[^\"]*\"\\)*\\)*\\)\\(.*\\)\\'" "\\1 -ini -no-pdf -interaction=nonstopmode \"&\\1\" " preview-format-name ".ini \\5"))))
 '(preview-dvipng-command "dvisvgm %d -o \"%m/prev%%03d.svg\"")
 '(preview-dvipng-image-type (quote svg))
'(projectile-globally-ignored-directories
(quote
 (".idea" ".eunit" ".git" ".hg" ".fslckout" ".bzr" "_darcs" ".tox" ".svn" ".stack-work" ".psci_modules" "cabal-sandbox" "output" "dist" "build" ".cabal-sandbox" "auto" ".hakyll" "")))
'(projectile-globally-ignored-files
(quote
 ("*TAGS" "*~" "cabal.sandbox.config" ".dir-locals.el" ".git")))
 '(psc-ide-rebuild-on-save nil)
 '(purescript-font-lock-symbols nil)
 '(purescript-indent-spaces 4)
 '(purescript-indentation-ifte-offset 4)
 '(purescript-indentation-layout-offset 4)
 '(purescript-indentation-left-offset 4)
 '(purescript-indentation-starter-offset 4)
 '(purescript-indentation-where-post-offset 4)
 '(purescript-indentation-where-pre-offset 4)
 '(py--imenu-create-index-function (quote py--imenu-create-index-new))
 '(py--imenu-create-index-p t)
 '(py-auto-fill-mode t)
 '(py-comment-auto-fill-p t)
 '(py-company-pycomplete-p nil)
 '(py-docstring-style (quote pep-257))
 '(py-electric-close-active-p t)
 '(py-electric-colon-active-p nil)
 '(py-electric-comment-p t)
 '(py-empty-line-closes-p t)
 '(py-fontify-shell-buffer-p t)
 '(py-hide-show-minor-mode-p t)
 '(py-highlight-error-source-p t)
 '(py-honor-IPYTHONDIR-p t)
 '(py-honor-PYTHONHISTORY-p t)
 '(py-imenu-show-method-args-p t)
 '(py-indent-honors-inline-comment t)
 '(py-indent-honors-multiline-listing t)
 '(py-ipython-command "ipython3")
 '(py-ipython-command-args "--pylab --automagic --gui=qt")
 '(py-load-skeletons-p nil)
 '(py-mark-decorators t)
 '(py-match-paren-mode t)
 '(py-org-cycle-p t)
 '(py-python-command "python3")
 '(py-python-edit-version "python3")
 '(py-return-key (quote py-newline-and-indent))
 '(py-set-pager-cat-p t)
 '(py-sexp-function (quote py-end-of-expression))
 '(py-sexp-use-expression-p t)
 '(py-shell-name "ipython3")
 '(py-shell-toggle-1 "python3")
 '(py-shell-toggle-2 "python2")
 '(py-smart-indentation t)
 '(py-smart-operator-mode-p t)
 '(py-tab-indents-region-p t)
 '(py-tab-shifts-region-p t)
 '(py-trailing-whitespace-smart-delete-p t)
 '(py-uncomment-indents-p t)
 '(py-use-local-default t)
 '(python-environment-virtualenv (quote ("virtualenv" "--quiet")))
'(python-mode-hook
(quote
 ((lambda nil
    (set
     (make-local-variable
      (quote yas-indent-line))
     (quote fixed)))
  #[0 "
\211\207"
      [py--imenu-create-index-function imenu-create-index-function py-indent-tabs-mode indent-tabs-mode]
      2]
  #[0 "\303
\304\305#\210	\211\207"
      [abbrevs-changed py-this-abbrevs-changed abbrev-file-name load nil t]
      4])) t)
 '(python-shell-interpreter "python3.5m")
 '(rainbow-identifiers-cie-l*a*b*-lightness 30)
 '(rainbow-identifiers-cie-l*a*b*-saturation 35)
'(recentf-save-file
(concat
 (emacs-run-data-dir "recentf" "recent-save")
 "recentf"))
 '(ruler-mode-basic-graduation-char 8901)
 '(ruler-mode-tab-stop-char 8595)
'(safe-local-variable-values
(quote
 ((standard-indent . 2)
  (projectile-project-compilation-cmd . "sbt compile")
  (projectile-project-compilation-cmd . "./sbt compile")
  (projectile-project-compilation-cmd . "cmake --build .")
  (projectile-project-compilation-cmd . "./node_modules/.bin/psc-package build")
  (projectile-project-compilation-dir . "build")
  (projectile-project-compilation-cmd . "make -j 4")
  (projectile-project-build-cmd . "make -j 4")
  (projectile-project-build-dir . "/build")
  (projectile-project-compilation-cmd . "cmake .. -DCMAKE_INSTALL_PREFIX=/opt/visualization -DCMAKE_BUILD_TYPE=Release")
  (projectile-project-compilation-cmd . "./hcc-build.sh")
  (projectile-project-compilation-cmd . "cmake .. -DCMAKE_INSTALL_PREFIX=/opt/visualization -DCMAKE_BUILD_TYPE=Release -G Ninja && ninja")
  (projectile-project-compilation-dir . "/build")
  (js2-additional-externs "$" "document")
  (js2-additional-externs quote
                          ("$" "document"))
  (js2-global-externs list "$" "document")
  (js2-global-externs "$" "document")
  (js2-global-externs "$"
                      (\, "document"))
  (js2-global-externs "document")
  (projectile-project-compilation-cmd . "make -j4 -k")
  (projectile-project-compilation-dir . "/build_unix")
  (projectile-project-compilation-cmd . "make -j4")
  (org-bibtex-entries . references\.bib)
  (lentic-init . lentic-orgel-org-init)
  (projectile-project-compilation-cmd . "./install.sh")
  (projectile-project-compilation-dir "/")
  (projectile-project-compilation-cmd "make")
  (projectile-project-compilation-dir "/build")
  (projectile-project-compilation-cmd . "cabal new-build all")
  (sh-shell . "zsh")
  (projectile-project-test-cmd . "./tests.sh")
  (projectile-project-compilation-cmd . "cabal new-build --enable-tests")
  (projectile-project-compilation-cmd . "psc-package build")
  (TeX-engine . LuaTex)
  (eval
   (add-hook
    (quote projectile-after-switch-project-hook)
    (function psc-ide-server-start)))
  (projectile-project-compilation-cmd lambda nil
                                      (byte-recompile-directory
                                       (projectile-compilation-dir)
                                       0))
  (projectile-project-compilation-dir . "/init.d")
  (projectile-project-compilation-cmd lambda nil
                                      (async-byte-recompile-directory
                                       (projectile-compilation-dir)))
  (projectile-project-test-cmd . "PATH=./node_modules/.bin:$PATH pulp test")
  (projectile-project-compile-cmd . "PATH=./node_modules/.bin:$PATH pulp build")
  (projectile-project-test-cmd . "pulp test")
  (projectile-project-compile-cmd . "pulp build")
  (projectile-project-compilation-cmd . "./node_modules/.bin/webpack --config ./webpack.config.js --progress --profile --bail")
  (projectile-project-compilation-cmd . "npm run build:emacs")
  (whitespace-style)
  (whitespace-style face trailing tabs spaces empty)
  (projectile-globally-ignored-files append
                                     ("elpa" "server" "var")
                                     (quote projectile-globally-ignored-files))
  (projectile-project-compilation-cmd . "/opt/ghc/bin/cabal new-build")
  (projectile-project-compilation-cmd . "npm run build")
  (projectile-project-compile-dir . "/")
  (projectile-project-compile-cmd . "npm run build")
  (projectile-project-compilation-dir . "/Documentation/")
  (projectile-project-compilation-dir . "/Documentation")
  (projectile-project-compilation-dir . "/FunctionalWebExamples")
  (org-use-sub-superscripts)
  (eval
   (progn
     (make-file-local-variable "py-shell-local-path"
                               (concat
                                (projectile-project-root)
                                "venv/bin/python"))
     (make-file-local-variable "python-shell-virtualenv-root"
                               (concat
                                (projectile-project-root)
                                "venv"))))
  (python-shell-virtualenv-root
   (progn
     (concat
      (projectile-project-root)
      "venv")))
  (py-shell-local-path
   (progn
     (concat
      (projectile-project-root)
      "venv/bin/python")))
  (py-shell-local-path
   (concat
    (projectile-project-root)
    "venv/bin/python"))
  (python-shell-virtualenv-root
   (concat
    (projectile-project-root)
    "venv"))
  (nil)
  (python-shell-virtualenv-path
   (concat
    (projectile-project-root)
    "venv"))
  (projectile-project-compilation-cmd . "make")
  (nameless-current-name . "biblio")
  (TeX-engine quote luatex)
  (projectile-project-root . ".")
  (haskell-process-type . cabal-new-repl)
  (projectile-project-compilation-cmd . "cabal new-build")
  (haskell-process-type quote cabal-new-repl)
  (checkdoc-minor-mode . t)
  (mangle-whitespace . t)
  (buffer-file-coding-system . utf-8-unix)
  (bug-reference-bug-regexp . "#\\(?2:[0-9]+\\)")
  (eval progn
        (setenv "TPG_DB" "npg")
        (setenv "TPG_SOCK" "/var/run/postgresql/.s.PGSQL.5432"))
  (eval progn
        (set-env "TPG_DB" "npg")
        (set-env "TPG_SOCK" "/var/run/postgresql/.s.PGSQL.5432"))
  (eval setenv "TPG_SOCK" "/var/run/postgresql/.s.PGSQL.5432")
  (eval setenv "TPG_USER" "$USER" t)
  (eval setenv "TPG_DB" "$USER" t)
  (epa-file-encrypt-to . evan@navipointgenomics\.com)
  (projectile-project-test-cmd lambda nil
                               (haskell-process-do-cabal "test --show-details=always --test-options=\"-a 25000 --maximum-unsuitable-generated-tests=100000 --maximum-test-depth=100000 --threads=4 +RTS -N\""))
  (projectile-project-test-cmd lambda nil
                               (haskell-process-do-cabal "test --show-details=always --test-options=\"-a 25000 --maximum-unsuitable-generated-tests=100000 --maximum-test-depth=100000\""))
  (TeX-master . Infrastructureless)
  (projectile-project-test-cmd lambda nil
                               (haskell-process-do-cabal "test --show-details=always"))
  (projectile-project-compilation-cmd lambda nil
                                      (haskell-process-cabal-build)
                                      (haskell-process-add-cabal-autogen))
  (projectile-project-test-cmd lambda nil
                               (haskell-process-do-cabal "test"))
  (eval progn
        (haskell-interactive-switch))
  (eval setq byte-compile-not-obsolete-vars
        (quote
         (display-buffer-function)))
  (projectile-project-compilation-cmd . "pulp --monochrome build")
  (projectile-project-compilation-cmd . "pulp build")
  (projectile-project-compilation-cmd . "pulp browserify --monochrome --to test.js")
  (projectile-project-compilation-cmd . "pulp browserify --to test.js")
  (projectile-project-compilation-cmd . "cabal run site-builder clean && cabal run site-builder build")
  (projectile-project-compilation-cmd lambda nil
                                      (haskell-process-do-cabal "build")
                                      (haskell-process-add-cabal-autogen))
  (projectile-project-compilation-cmd quote haskell-process-cabal-build)
  (eval haskell-interactive-switch)
  (eval
   (haskell-interactive-switch))
  (projectile-project-compilation-cmd lambda nil
                                      (haskell-process-cabal "build"))
  (projectile-project-compilation-cmd function haskell-process-cabal-build)
  (projectile-project-compilation-cmd . haskell-compile)
  (projectile-project-compilation-dir projectile-project-root)
  (projectile-project-compilation-cmd . "npm --color=no run compile:debug")
  (projectile-project-compilation-cmd . "cabal build")
  (projectile-project-compilation-cmd . "cabal build && npm run compile:debug")
  (projectile-project-compilation-cmd function haskell-compile)
  (projectile-project-compilation-cmd . "cabal run build")
  (projectile-project-compilation-cmd . "webpack --no-color --debug")
  (projectile-project-compilation-cmd . "npm --color=false run compile:debug")
  (projectile-project-compilation-cmd . "npm run compile:debug")
  (projectile-project-compilation-dir . "/")
  (projectile-project-compilation-cmd . "npm run compile")
  (projectile-project-compilation-cmd . "npm run webpack")
  (projectile-project-compilation-cmd . "cabal run np-uploader build")
  (header-auto-update-enabled)
  (projectile-project-compilation-cmd format "cd %s && PATH=/opt/cabal/1.24/bin:/opt/ghc/8.0.2/bin cabal run np-uploader build"
                                      (projectile-project-root))
  (projectile-globally-ignored-directories quote
                                           ("node_modules" "bower_components"))
  (projectile-project-compilation-cmd format "cd %s && cabal run np-uploader build"
                                      (projectile-project-root))
  (projectile-project-compilation-cmd lambda nil
                                      (compilation-start
                                       (format "cd %s && pulp build --build-path build/webpack/purs"
                                               (projectile-project-root))))
  (projectile-project-compilation-cmd lambda
                                      (root)
                                      (compilation-start
                                       (format "cd %s && pulp build --build-path build/webpack/purs" root)))
  (projectile-project-compilation-cmd lambda nil
                                      (interactive)
                                      (compilation-start "cd ../../ && pulp build --build-path build/webpack/purs"))
  (projectile-project-compilation-cmd lambda nil
                                      (compilation-start "cd ../../ && pulp build --build-path build/webpack/purs"))
  (projectile-project-compilation-cmd "export PATH=./node_modules/.bin:$PATH; cd ../../ && pulp build --build-path build/webpack/purs")
  (projectile-compilation-dir . "/projects/navipoint/HaskellProject")
  (projectile-project-compilation-cmd . "./bin/build.sh")
  (projectile-tags-command . "echo :etags | cabal repl")
  (projectile-tags-backend quote etags)
  (global-set-key
   (kbd "<s-return>")
   (quote haskell-compile))
  (eval highlight-regexp "^ *")
  (TeX-master . "../HaskellWeb")
  (TeX-master . t)
  (projectile-project-compilation-cmd . "export PATH=./node_modules/.bin:$PATH; cd ../../ && pulp build --build-path build/webpack/purs"))))
 '(save-interprogram-paste-before-kill t)
 '(scalable-fonts-allowed t)
 '(scroll-bar-mode (quote right))
 '(select-enable-primary t)
 '(semantic-imenu-auto-rebuild-directory-indexes t)
 '(semantic-imenu-index-directory t)
 '(semantic-imenu-sort-bucket-function (quote semantic-sort-tags-by-name-increasing-ci))
 '(semantic-imenu-summary-function (quote semantic-format-tag-concise-prototype))
 '(show-paren-style (quote mixed))
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#eee8d5" 0.2))
 '(smerge-auto-leave nil)
 '(smerge-command-prefix "")
 '(smerge-diff-buffer-name "*smerge-diff*")
 '(sql-connection-alist (quote (("evan" (sql-product (quote postgres))))))
 '(sql-input-ring-file-name "~/.emacs.d/sqli.history")
 '(sql-product (quote postgres))
 '(tabbar-background-color "#ffffff")
 '(tool-bar-mode nil)
 '(truncate-partial-width-windows t)
 '(tum-emacs-js-global-bin-dir "/opt/npm-packages/bin/")
 '(tum-emacs-js-global-dir "/opt/npm-packages/")
 '(undo-limit 33560192)
 '(undo-outer-limit 1048576)
 '(undo-strong-limit 1048576)
 '(undo-tree-auto-save-history nil)
 '(undo-tree-mode-lighter " UT")
 '(undo-tree-visualizer-diff t)
 '(undo-tree-visualizer-lazy-drawing 35)
 '(undo-tree-visualizer-timestamps nil)
 '(user-mail-address "evan@theunixman.com")
 '(vc-annotate-background nil)
'(vc-annotate-color-map
(quote
 ((20 . "#dc322f")
  (40 . "#cb4b16")
  (60 . "#b58900")
  (80 . "#859900")
  (100 . "#2aa198")
  (120 . "#268bd2")
  (140 . "#d33682")
  (160 . "#6c71c4")
  (180 . "#dc322f")
  (200 . "#cb4b16")
  (220 . "#b58900")
  (240 . "#859900")
  (260 . "#2aa198")
  (280 . "#268bd2")
  (300 . "#d33682")
  (320 . "#6c71c4")
  (340 . "#dc322f")
  (360 . "#cb4b16"))))
 '(vc-annotate-very-old-color nil)
 '(vc-display-status nil)
 '(vc-follow-symlinks t)
 '(visual-line-fringe-indicators (quote (nil right-curly-arrow)))
 '(vm-toolbar-pixmap-directory "/usr/share/emacs24/site-lisp/vm/pixmaps")
 '(wakatime-python-bin nil)
 '(wdired-use-dired-vertical-movement t)
 '(whitespace-action (quote (auto-cleanup)))
 '(whitespace-line-column 160)
 '(word-wrap nil)
 '(xgit-log-max-count 20)
 '(xgit-mail-notification-sign-off-p t)
 '(xgit-use-index (quote always)))
